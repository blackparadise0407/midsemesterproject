//      ===========================LOADMOVIES ON REFRESH FUNCTION===========================

window.onload = function() {
    loadMoviesPopular();


}


//      ===========================LOADMOVIES FUNCTION===========================
function loadMoviesPopular() {
    loadingPage();

    const url = 'https://api.themoviedb.org/3/movie/popular?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&page=1';
    fetch(url)
        .then((resp) => resp.json())
        .then(function(data) {
            $('.row').empty();
            $('.list-unstyled').empty();
            let movies = data.results;

            // return movies.map(function(movie) {
            for (const movie of movies) {

                $('.row').append(`
                <div class="col-md-4 py-4 d-flex align-items-stretch ">
                    <div class="card text-white bg-dark mb-3 " style="max-width: 18rem;">
                    <img id="image" src="https://image.tmdb.org/t/p/w780/${movie.poster_path}" class="card-img-top" alt="${movie.title}" width="140px " height="440px ">
                        <div class="card-body ">
                            <h5 class="card-title ">${movie.title}</h5>
                            <p class="card-text float-right p-1 ">IMDB Rated: ${movie.vote_average}</p>
                            <br/>
                            <button class="btn btn-danger btn-sm mb-1" style="width: 150px;" type="submit" onclick=getMovieDetailUpdate(${movie.id})>CLICK FOR DETAIL</button>
                        </div>
                        <div class="card-footer text-muted ">
                            Release Date: ${movie.release_date}
                        </div>
                    </div>
                </div>`);
                // })

            }

        })
        .catch(function(error) {
            console.log(JSON.stringify(error));
        });
}

//      ===========================LOADMOVIES TOPRATED FUNCTION===========================
function loadMoviesToprated() {
    loadingPage();
    const url = 'https://api.themoviedb.org/3/movie/top_rated?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&page=1';
    fetch(url)
        .then((resp) => resp.json())
        .then(function(data) {
            $('.row').empty();
            $('.list-unstyled').empty();
            let movies = data.results;
            // return movies.map(function(movie) {
            for (const movie of movies) {

                $('.row').append(`
                <div class="col-md-4 py-4 d-flex align-items-stretch ">
                    <div class="card text-white bg-dark mb-3 " style="max-width: 18rem;">
                    <img id="image" src="https://image.tmdb.org/t/p/w780/${movie.poster_path}" class="card-img-top" alt="${movie.title}" width="140px " height="440px ">
                        <div class="card-body ">
                            <h5 class="card-title ">${movie.title}</h5>
                            <p class="card-text float-right p-1 ">IMDB Rated: ${movie.vote_average}</p>
                            <br/>
                            <button class="btn btn-danger btn-sm " style="width: 150px;" type="submit" onclick=getMovieDetailUpdate(${movie.id})>CLICK FOR DETAIL</button>
                        </div>
                        <div class="card-footer text-muted ">
                            Release Date: ${movie.release_date}
                        </div>
                    </div>
                </div>`);
                // })
            }
        })
        .catch(function(error) {
            console.log(JSON.stringify(error));
        });
}

//      ===========================LOADMOVIES NOWPLAYING FUNCTION===========================
function loadMoviesNowplaying() {
    loadingPage();
    const url = 'https://api.themoviedb.org/3/movie/now_playing?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&page=1';
    fetch(url)
        .then((resp) => resp.json())
        .then(function(data) {
            $('.row').empty();
            $('.list-unstyled').empty();
            let movies = data.results;
            // return movies.map(function(movie) {
            for (const movie of movies) {

                $('.row').append(`
                <div class="col-md-4 py-4 d-flex align-items-stretch ">
                    <div class="card text-white bg-dark mb-3 " style="max-width: 18rem;">
                    <img id="image" src="https://image.tmdb.org/t/p/w780/${movie.poster_path}" class="card-img-top" alt="${movie.title}" width="140px " height="440px ">
                        <div class="card-body ">
                            <h5 class="card-title ">${movie.title}</h5>
                            <p class="card-text float-right p-1 ">IMDB Rated: ${movie.vote_average}</p>
                            <br/>
                            <button class="btn btn-danger btn-sm " style="width: 150px;" type="submit" onclick=getMovieDetailUpdate(${movie.id})>CLICK FOR DETAIL</button>
                        </div>
                        <div class="card-footer text-muted ">
                            Release Date: ${movie.release_date}
                        </div>
                    </div>
                </div>`);
                // })
            }
        })
        .catch(function(error) {
            console.log(JSON.stringify(error));
        });
}


//      ===========================SEARCHMOVIES BY NAME FUNCTION===========================
async function eventSubmitByMovie(event) {
    event.preventDefault();
    var value;

    $("input")
        .keyup(function() {
            value = $(this).val();
        })
        .keyup();

    loadingPage();

    const reqStr = `https://api.themoviedb.org/3/search/movie?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&query=${value}&page=1&include_adult=false`;

    fetch(reqStr)
        .then((resq) => resq.json())
        .then(function(data) {
            $('.row').empty();
            $('.list-unstyled').empty();
            let movies = data.results;
            for (const movie of movies) {

                $('.row').append(`
            <div class="col-md-4 py-4 d-flex align-items-stretch ">
                    <div class="card text-white bg-dark mb-3 " style="max-width: 18rem;">
                    <img src="https://image.tmdb.org/t/p/w780/${movie.poster_path} " class="card-img-top " alt="${movie.title} " width="140px " height="440px ">
                        <div class="card-body ">
                            <h5 class="card-title ">${movie.title}</h5>
                            <p class="card-text float-right p-1 ">IMDB Rated: ${movie.vote_average}</p>
                            <br/>
                            <button class="btn btn-danger btn-sm " style="width: 150px;" type="submit" onclick=getMovieDetailUpdate(${movie.id})>CLICK FOR DETAIL</button>
                        </div>
                        <div class="card-footer text-muted ">
                            Release Date: ${movie.release_date}
                        </div>
                </div>
            </div>`);

            }

        })
        .catch(function(error) {
            $('.row').empty();
            $('.list-unstyled').empty();
            $('.row').append(`
            <div class="alert alert-danger mx-auto mb-1 p-3" role="alert">
                Sorry, no movies matched your search. Please try again.
            </div>
            `)

            console.log(error);
        });
}


//      ===========================SEARCHMOVIES BY ACTORS FUNCTION====================
async function eventSubmitByActor(event) {
    event.preventDefault();
    var value;

    $("input")
        .keyup(function() {
            value = $(this).val();
        })
        .keyup();
    loadingPage();

    const reqStr = `https://api.themoviedb.org/3/search/person?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&query=${value}&page=1&include_adult=false`;


    fetch(reqStr)
        .then((resq) => resq.json())
        .then(function(data) {
            $('.row').empty();
            $('.list-unstyled').empty();
            let actors = data.results;
            console.log(actors)

            for (const actor of actors) {
                $('.row').append(`
                <div class="col-md-4 py-4 d-flex align-items-stretch ">
                    <div class="card text-white bg-dark mb-3 " style="max-width: 18rem;">
                    <img src="https://image.tmdb.org/t/p/w780/${actor.known_for[0].poster_path} " class="card-img-top " alt="${actor.known_for[0].title} " width="140px " height="440px ">
                        <div class="card-body ">
                            <h5 class="card-title ">${actor.known_for[0].title}</h5>
                            <p class="card-text float-right p-1 ">IMDB Rated: ${actor.known_for[0].vote_average}</p>
                            <br/><br/><br/>
                            <p class="card-text p-1 ">Actor: ${actor.name}</p>
                            <br/>
                            <button class="btn btn-danger btn-sm " style="width: 150px;" type="submit" onclick=getMovieDetailUpdate(${actor.known_for[0].id})>CLICK FOR DETAIL</button>
                        </div>
                        <div class="card-footer text-muted ">
                            Release Date: ${actor.known_for[0].release_date}
                        </div>
                    </div>
                </div>`);
                document.getElementById('loading').innerHTML = "";
            }

        })
        .catch(function(error) {
            $('.row').empty();
            $('.list-unstyled').empty();
            $('.row').append(`
            <div class="alert alert-danger mx-auto mb-1 p-3" role="alert">
                Sorry, no actors matched your search. Please try again.
            </div>
            `)
            console.log(error);
        });
}




//      ===========================SEARCH ACTORS FUNCTION===========================
async function eventSubmit2(event) {
    event.preventDefault();
    var value;

    $("input")
        .keyup(function() {
            value = $(this).val();
        })
        .keyup();

    console.log(value)
    loadingPage();
    const reqStr = `https://api.themoviedb.org/3/search/person?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&query=${value}&page=1&include_adult=false`;

    fetch(reqStr)
        .then((resq) => resq.json())
        .then(function(data) {
            $('.row').empty();
            $('.list-unstyled').empty();
            let actors = data.results;
            console.log(actors)
            for (const actor of actors) {
                $('.list-unstyled').append(`
                <li class="media p-3 border border-dark mb-2 bg-light text-dark rounded">
                    <img class="mr-3 border border-dark" src="https://image.tmdb.org/t/p/w185/${actor.profile_path}" alt="${actor.name}">
                    <div class="media-body">
                        <h5 class="mt-0 mb-1">${actor.name}</h5>
                        Known for: ${actor.known_for_department}
                        <br/>
                        <img class="mr-3 border border-dark float-right" src="https://image.tmdb.org/t/p/w185/${actor.known_for[0].poster_path}" alt="${actor.known_for[0].title}" height="230px" width="180px">
                        
                        
                        Movies: ${actor.known_for[0].title}
                        <br/>
                        <div class="float-left mb-1">Released Date: ${actor.known_for[0].release_date}</div>
                        
                    </div>
                    <div class="media-footer">Popularity: ${actor.popularity}</div>
                </li>
            `);
            }
        })
        .catch(function(error) {
            $('.row').empty();
            $('.list-unstyled').empty();
            $('.list-unstyled').append(`
            <div class="alert alert-danger mx-auto mb-1 p-3" role="alert">
                Sorry, no actors matched your search. Please try again.
            </div>
            `)
            console.log(error);
        });
}


// ==========================GETACTORDETAIL FUNCTION===============================
async function getActorDetail(id) {
    loadingPage();
    const actor_url = `https://api.themoviedb.org/3/person/${id}?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US`;
    fetch(actor_url)
        .then((resq) => resq.json())
        .then((json) => {
            let actor = json;

            const reqStr = `https://api.themoviedb.org/3/search/person?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&query=${actor.name}&page=1&include_adult=false`;

            fetch(reqStr)
                .then((resq) => resq.json())
                .then((json) => {
                    let actordetail = json.results;
                    console.log(actordetail)


                    $('.row').empty();
                    $('.list-unstyled').empty();
                    $('.row').append(`
                <div class="row no-gutters bg-light position-relative shadow p-1 bg-white rounded">
                    <div class="col-md-6 mb-md-0 p-md-4">
                        <img src="https://image.tmdb.org/t/p/w780/${actor.profile_path}" class=" shadow bg-white rounded style="max-width: 29rem;"" alt="${actor.name}" height="680px" width="440px">
                    </div>
                    <div class="col-md-6 position-static p-4 pl-md-0">
                        <h2>
                            ${actor.name}
                        </h2>
                        <hr/>
                        <p>Birthday: ${actor.birthday}</p>
                        <p>Biography: ${actor.biography}</p>
                        <div class="card-deck px-2">
                            <div class="card bg-dark text-white" style="max-width: 8rem; max-height: 16rem;">
                                <img src="https://image.tmdb.org/t/p/w780/${actordetail[0].known_for[0].poster_path}" class="card-img-top" alt="${actordetail[0].known_for[0].title}">
                                <div class="card-body">
                                    <p class="card-title">${actordetail[0].known_for[0].title}</p>
                                    <a class="stretched-link" style="cursor: pointer;"  onclick=getMovieDetailUpdate(${actordetail[0].known_for[0].id})></a>
                                </div>
                            </div>
                            <div class="card bg-dark text-white" style="max-width: 8rem; max-height: 16rem;">
                                <img src="https://image.tmdb.org/t/p/w780/${actordetail[0].known_for[1].poster_path}" class="card-img-top" alt="${actordetail[0].known_for[1].title}">
                                <div class="card-body">
                                    <a class="stretched-link" style="cursor: pointer;"  onclick=getMovieDetailUpdate(${actordetail[0].known_for[1].id})></a>
                                    <p class="card-title">${actordetail[0].known_for[1].title}</p>
                                </div>
                            </div>
                        </div>


                

                        
                        <a href="index.html" class="btn btn-danger btn-sm p-1 float-right" style="width: 150px;" role="button" aria-pressed="true">Back to home page</a>
                    </div>
                </div>
            `)


                })
        })
        .catch(function(error) {
            $('.row').empty();
            $('.list-unstyled').empty();
            $('.list-unstyled').append(`
            <div class="alert alert-danger mx-auto" style="width: 600px;" role="alert">
                ERROR
            </div>
            `)
            console.log(error);
        });
}

//          ============================GETREVIEW FUNCTION==============================

async function getReview(id) {
    loadingPage();
    rev_url = `https://api.themoviedb.org/3/movie/${id}/reviews?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&page=1`
    fetch(rev_url)
        .then((resp) => resp.json())
        .then(json => {
            let reviews = json.results;
            console.log(reviews)
            for (const review of reviews) {
                $('.row').append(`
                <p>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false"      aria-controls="collapseExample">Show Review</button>
                </p>
                <br/>
                <div class="collapse" id="collapseExample">
                    <div class="card card-body">${review.content}</div>
                </div>
               
                `)
            }

        })
        .catch(function(error) {
            $('.row').empty();
            $('.list-unstyled').empty();
            $('.list-unstyled').append(`
            <div class="alert alert-danger mx-auto" style="width: 600px;" role="alert">
                ERROR
            </div>
            `)
            console.log(error);
        });
}

// ==============================UPDATEGETMOVIEDETAIL FUNCTION==================================
async function getMovieDetailUpdate(id) {
    loadingPage();
    const crd_url = `https://api.themoviedb.org/3/movie/${id}/credits?api_key=bd2d4bb5106832ed13fb8fe28dd20430`;
    fetch(crd_url)
        .then((resq) => resq.json())
        .then((json) => {
            let credit = json;
            const detail_url = `https://api.themoviedb.org/3/movie/${id}?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US`;
            fetch(detail_url)
                .then((res) => res.json())
                .then((json) => {
                    $('.row').empty();
                    $('.list-unstyled').empty();
                    let data = json;
                    rev_url = `https://api.themoviedb.org/3/movie/${id}/reviews?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&page=1`
                    fetch(rev_url)
                        .then((resp) => resp.json())
                        .then(json => {
                            let reviews = json.results;

                            $('.row').append(`
                    <div class="row no-gutters bg-light position-relative shadow p-1 bg-white rounded">
                    <div class="col-md-6 mb-md-0 p-md-4">
                        <img src="https://image.tmdb.org/t/p/w780/${data.poster_path}" class=" shadow bg-white rounded style="max-width: 29rem;"" alt="${data.title}" height="680px" width="440px">
                    </div>
                    <div class="col-md-6 position-static p-4 pl-md-0">
                        <h1 class=" " role="alert">
                            ${data.title}
                        </h1>
                        <hr/>
                        <p>Genre: ${data.genres[0].name}, ${data.genres[1].name}</p>
                        <p>Overview: ${data.overview}</p>
                        <p>Actors: <a class="text-primary link" style="cursor: pointer;" onclick=getActorDetail(${credit.cast[0].id})>${credit.cast[0].name}</a>, <a class="text-primary link" style="cursor: pointer;"  onclick=getActorDetail(${credit.cast[1].id})>${credit.cast[1].name}</a>, <a class="text-primary link" style="cursor: pointer;"  onclick=getActorDetail(${credit.cast[2].id})>${credit.cast[2].name}</a>, <a class="text-primary link" style="cursor: pointer;"  onclick=getActorDetail(${credit.cast[3].id})>${credit.cast[3].name}</a>...</p>
                        <p>Crews: ${credit.crew[0].name}, ${credit.crew[1].name}, ${credit.crew[2].name}...</p>
                        <p>Original language: ${data.original_language}</p>
                        <p>Spoken languages: ${data.spoken_languages[0].name}</p>
                        <p>Length: ${data.runtime} minutes</p>
                        <p>Released date: ${data.release_date}</p>
                        <p>Production countries: ${data.production_countries[0].name}</p>
                        
                        
                        
                        <a href="index.html" class="btn btn-danger btn-sm p-1 float-right" style="width: 150px;" role="button" aria-pressed="true">Back to home page</a>
                        </div>
                        <p>
                        <button class="btn btn-danger ml-4" type="button" data-toggle="collapse" style="width: 200px;" data-target="#reviewCollapse" aria-expanded="false"   aria-controls="collapseExample">Show/Hide Review</button>
                        </p>
                        <div class="collapse" id="reviewCollapse">
                        <div class="card card-body ml-4 mr-4 mb-4">
                        <p>Authors: ${reviews[0].author}</p>
                        <hr/>
                        <p>${reviews[0].content}</p>
                        </div>
                        </div>
                </div>
            `)

                        })
                })
        })
        .catch(function(error) {
            $('.row').empty();
            $('.list-unstyled').empty();
            $('.list-unstyled').append(`
            <div class="alert alert-danger mx-auto mb-1 p-3" role="alert">
                ERROR
            </div>
            `)
            console.log(error);
        });
}

function loadingPage() {
    $('.row').empty();
    $('.row').append(`
    <div class="d-flex justify-content-center w-100">
    <div class="spinner-grow text-danger" style="width: 4rem; height: 4rem;" role="status">
    <span class="sr-only">Loading...</span>
  </div>
  </div>
      `);
}//      ===========================LOADMOVIES ON REFRESH FUNCTION===========================

window.onload = function() {
    loadMoviesPopular();


}


//      ===========================LOADMOVIES FUNCTION===========================
function loadMoviesPopular() {
    loadingPage();

    const url = 'https://api.themoviedb.org/3/movie/popular?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&page=1';
    fetch(url)
        .then((resp) => resp.json())
        .then(function(data) {
            $('.row').empty();
            $('.list-unstyled').empty();
            let movies = data.results;

            // return movies.map(function(movie) {
            for (const movie of movies) {

                $('.row').append(`
                <div class="col-md-4 py-4 d-flex align-items-stretch ">
                    <div class="card text-white bg-dark mb-3 " style="max-width: 18rem;">
                    <img id="image" src="https://image.tmdb.org/t/p/w780/${movie.poster_path}" class="card-img-top" alt="${movie.title}" width="140px " height="440px ">
                        <div class="card-body ">
                            <h5 class="card-title ">${movie.title}</h5>
                            <p class="card-text float-right p-1 ">IMDB Rated: ${movie.vote_average}</p>
                            <br/>
                            <button class="btn btn-danger btn-sm mb-1" style="width: 150px;" type="submit" onclick=getMovieDetailUpdate(${movie.id})>CLICK FOR DETAIL</button>
                        </div>
                        <div class="card-footer text-muted ">
                            Release Date: ${movie.release_date}
                        </div>
                    </div>
                </div>`);
                // })

            }

        })
        .catch(function(error) {
            console.log(JSON.stringify(error));
        });
}

//      ===========================LOADMOVIES TOPRATED FUNCTION===========================
function loadMoviesToprated() {
    loadingPage();
    const url = 'https://api.themoviedb.org/3/movie/top_rated?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&page=1';
    fetch(url)
        .then((resp) => resp.json())
        .then(function(data) {
            $('.row').empty();
            $('.list-unstyled').empty();
            let movies = data.results;
            // return movies.map(function(movie) {
            for (const movie of movies) {

                $('.row').append(`
                <div class="col-md-4 py-4 d-flex align-items-stretch ">
                    <div class="card text-white bg-dark mb-3 " style="max-width: 18rem;">
                    <img id="image" src="https://image.tmdb.org/t/p/w780/${movie.poster_path}" class="card-img-top" alt="${movie.title}" width="140px " height="440px ">
                        <div class="card-body ">
                            <h5 class="card-title ">${movie.title}</h5>
                            <p class="card-text float-right p-1 ">IMDB Rated: ${movie.vote_average}</p>
                            <br/>
                            <button class="btn btn-danger btn-sm " style="width: 150px;" type="submit" onclick=getMovieDetailUpdate(${movie.id})>CLICK FOR DETAIL</button>
                        </div>
                        <div class="card-footer text-muted ">
                            Release Date: ${movie.release_date}
                        </div>
                    </div>
                </div>`);
                // })
            }
        })
        .catch(function(error) {
            console.log(JSON.stringify(error));
        });
}

//      ===========================LOADMOVIES NOWPLAYING FUNCTION===========================
function loadMoviesNowplaying() {
    loadingPage();
    const url = 'https://api.themoviedb.org/3/movie/now_playing?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&page=1';
    fetch(url)
        .then((resp) => resp.json())
        .then(function(data) {
            $('.row').empty();
            $('.list-unstyled').empty();
            let movies = data.results;
            // return movies.map(function(movie) {
            for (const movie of movies) {

                $('.row').append(`
                <div class="col-md-4 py-4 d-flex align-items-stretch ">
                    <div class="card text-white bg-dark mb-3 " style="max-width: 18rem;">
                    <img id="image" src="https://image.tmdb.org/t/p/w780/${movie.poster_path}" class="card-img-top" alt="${movie.title}" width="140px " height="440px ">
                        <div class="card-body ">
                            <h5 class="card-title ">${movie.title}</h5>
                            <p class="card-text float-right p-1 ">IMDB Rated: ${movie.vote_average}</p>
                            <br/>
                            <button class="btn btn-danger btn-sm " style="width: 150px;" type="submit" onclick=getMovieDetailUpdate(${movie.id})>CLICK FOR DETAIL</button>
                        </div>
                        <div class="card-footer text-muted ">
                            Release Date: ${movie.release_date}
                        </div>
                    </div>
                </div>`);
                // })
            }
        })
        .catch(function(error) {
            console.log(JSON.stringify(error));
        });
}


//      ===========================SEARCHMOVIES BY NAME FUNCTION===========================
async function eventSubmitByMovie(event) {
    event.preventDefault();
    var value;

    $("input")
        .keyup(function() {
            value = $(this).val();
        })
        .keyup();

    loadingPage();

    const reqStr = `https://api.themoviedb.org/3/search/movie?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&query=${value}&page=1&include_adult=false`;

    fetch(reqStr)
        .then((resq) => resq.json())
        .then(function(data) {
            $('.row').empty();
            $('.list-unstyled').empty();
            let movies = data.results;
            for (const movie of movies) {

                $('.row').append(`
            <div class="col-md-4 py-4 d-flex align-items-stretch ">
                    <div class="card text-white bg-dark mb-3 " style="max-width: 18rem;">
                    <img src="https://image.tmdb.org/t/p/w780/${movie.poster_path} " class="card-img-top " alt="${movie.title} " width="140px " height="440px ">
                        <div class="card-body ">
                            <h5 class="card-title ">${movie.title}</h5>
                            <p class="card-text float-right p-1 ">IMDB Rated: ${movie.vote_average}</p>
                            <br/>
                            <button class="btn btn-danger btn-sm " style="width: 150px;" type="submit" onclick=getMovieDetailUpdate(${movie.id})>CLICK FOR DETAIL</button>
                        </div>
                        <div class="card-footer text-muted ">
                            Release Date: ${movie.release_date}
                        </div>
                </div>
            </div>`);

            }

        })
        .catch(function(error) {
            $('.row').empty();
            $('.list-unstyled').empty();
            $('.row').append(`
            <div class="alert alert-danger mx-auto mb-1 p-3" role="alert">
                Sorry, no movies matched your search. Please try again.
            </div>
            `)

            console.log(error);
        });
}


//      ===========================SEARCHMOVIES BY ACTORS FUNCTION====================
async function eventSubmitByActor(event) {
    event.preventDefault();
    var value;

    $("input")
        .keyup(function() {
            value = $(this).val();
        })
        .keyup();
    loadingPage();

    const reqStr = `https://api.themoviedb.org/3/search/person?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&query=${value}&page=1&include_adult=false`;


    fetch(reqStr)
        .then((resq) => resq.json())
        .then(function(data) {
            $('.row').empty();
            $('.list-unstyled').empty();
            let actors = data.results;
            console.log(actors)

            for (const actor of actors) {
                $('.row').append(`
                <div class="col-md-4 py-4 d-flex align-items-stretch ">
                    <div class="card text-white bg-dark mb-3 " style="max-width: 18rem;">
                    <img src="https://image.tmdb.org/t/p/w780/${actor.known_for[0].poster_path} " class="card-img-top " alt="${actor.known_for[0].title} " width="140px " height="440px ">
                        <div class="card-body ">
                            <h5 class="card-title ">${actor.known_for[0].title}</h5>
                            <p class="card-text float-right p-1 ">IMDB Rated: ${actor.known_for[0].vote_average}</p>
                            <br/><br/><br/>
                            <p class="card-text p-1 ">Actor: ${actor.name}</p>
                            <br/>
                            <button class="btn btn-danger btn-sm " style="width: 150px;" type="submit" onclick=getMovieDetailUpdate(${actor.known_for[0].id})>CLICK FOR DETAIL</button>
                        </div>
                        <div class="card-footer text-muted ">
                            Release Date: ${actor.known_for[0].release_date}
                        </div>
                    </div>
                </div>`);
                document.getElementById('loading').innerHTML = "";
            }

        })
        .catch(function(error) {
            $('.row').empty();
            $('.list-unstyled').empty();
            $('.row').append(`
            <div class="alert alert-danger mx-auto mb-1 p-3" role="alert">
                Sorry, no actors matched your search. Please try again.
            </div>
            `)
            console.log(error);
        });
}




//      ===========================SEARCH ACTORS FUNCTION===========================
async function eventSubmit2(event) {
    event.preventDefault();
    var value;

    $("input")
        .keyup(function() {
            value = $(this).val();
        })
        .keyup();

    console.log(value)
    loadingPage();
    const reqStr = `https://api.themoviedb.org/3/search/person?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&query=${value}&page=1&include_adult=false`;

    fetch(reqStr)
        .then((resq) => resq.json())
        .then(function(data) {
            $('.row').empty();
            $('.list-unstyled').empty();
            let actors = data.results;
            console.log(actors)
            for (const actor of actors) {
                $('.list-unstyled').append(`
                <li class="media p-3 border border-dark mb-2 bg-light text-dark rounded">
                    <img class="mr-3 border border-dark" src="https://image.tmdb.org/t/p/w185/${actor.profile_path}" alt="${actor.name}">
                    <div class="media-body">
                        <h5 class="mt-0 mb-1">${actor.name}</h5>
                        Known for: ${actor.known_for_department}
                        <br/>
                        <img class="mr-3 border border-dark float-right" src="https://image.tmdb.org/t/p/w185/${actor.known_for[0].poster_path}" alt="${actor.known_for[0].title}" height="230px" width="180px">
                        
                        
                        Movies: ${actor.known_for[0].title}
                        <br/>
                        <div class="float-left mb-1">Released Date: ${actor.known_for[0].release_date}</div>
                        
                    </div>
                    <div class="media-footer">Popularity: ${actor.popularity}</div>
                </li>
            `);
            }
        })
        .catch(function(error) {
            $('.row').empty();
            $('.list-unstyled').empty();
            $('.list-unstyled').append(`
            <div class="alert alert-danger mx-auto mb-1 p-3" role="alert">
                Sorry, no actors matched your search. Please try again.
            </div>
            `)
            console.log(error);
        });
}


// ==========================GETACTORDETAIL FUNCTION===============================
async function getActorDetail(id) {
    loadingPage();
    const actor_url = `https://api.themoviedb.org/3/person/${id}?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US`;
    fetch(actor_url)
        .then((resq) => resq.json())
        .then((json) => {
            let actor = json;

            const reqStr = `https://api.themoviedb.org/3/search/person?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&query=${actor.name}&page=1&include_adult=false`;

            fetch(reqStr)
                .then((resq) => resq.json())
                .then((json) => {
                    let actordetail = json.results;
                    console.log(actordetail)


                    $('.row').empty();
                    $('.list-unstyled').empty();
                    $('.row').append(`
                <div class="row no-gutters bg-light position-relative shadow p-1 bg-white rounded">
                    <div class="col-md-6 mb-md-0 p-md-4">
                        <img src="https://image.tmdb.org/t/p/w780/${actor.profile_path}" class=" shadow bg-white rounded style="max-width: 29rem;"" alt="${actor.name}" height="680px" width="440px">
                    </div>
                    <div class="col-md-6 position-static p-4 pl-md-0">
                        <h2>
                            ${actor.name}
                        </h2>
                        <hr/>
                        <p>Birthday: ${actor.birthday}</p>
                        <p>Biography: ${actor.biography}</p>
                        <div class="card-deck px-2">
                            <div class="card bg-dark text-white" style="max-width: 8rem; max-height: 16rem;">
                                <img src="https://image.tmdb.org/t/p/w780/${actordetail[0].known_for[0].poster_path}" class="card-img-top" alt="${actordetail[0].known_for[0].title}">
                                <div class="card-body">
                                    <p class="card-title">${actordetail[0].known_for[0].title}</p>
                                    <a class="stretched-link" style="cursor: pointer;"  onclick=getMovieDetailUpdate(${actordetail[0].known_for[0].id})></a>
                                </div>
                            </div>
                            <div class="card bg-dark text-white" style="max-width: 8rem; max-height: 16rem;">
                                <img src="https://image.tmdb.org/t/p/w780/${actordetail[0].known_for[1].poster_path}" class="card-img-top" alt="${actordetail[0].known_for[1].title}">
                                <div class="card-body">
                                    <a class="stretched-link" style="cursor: pointer;"  onclick=getMovieDetailUpdate(${actordetail[0].known_for[1].id})></a>
                                    <p class="card-title">${actordetail[0].known_for[1].title}</p>
                                </div>
                            </div>
                        </div>


                

                        
                        <a href="index.html" class="btn btn-danger btn-sm p-1 float-right" style="width: 150px;" role="button" aria-pressed="true">Back to home page</a>
                    </div>
                </div>
            `)


                })
        })
        .catch(function(error) {
            $('.row').empty();
            $('.list-unstyled').empty();
            $('.list-unstyled').append(`
            <div class="alert alert-danger mx-auto" style="width: 600px;" role="alert">
                ERROR
            </div>
            `)
            console.log(error);
        });
}

//          ============================GETREVIEW FUNCTION==============================

async function getReview(id) {
    loadingPage();
    rev_url = `https://api.themoviedb.org/3/movie/${id}/reviews?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&page=1`
    fetch(rev_url)
        .then((resp) => resp.json())
        .then(json => {
            let reviews = json.results;
            console.log(reviews)
            for (const review of reviews) {
                $('.row').append(`
                <p>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false"      aria-controls="collapseExample">Show Review</button>
                </p>
                <br/>
                <div class="collapse" id="collapseExample">
                    <div class="card card-body">${review.content}</div>
                </div>
               
                `)
            }

        })
        .catch(function(error) {
            $('.row').empty();
            $('.list-unstyled').empty();
            $('.list-unstyled').append(`
            <div class="alert alert-danger mx-auto" style="width: 600px;" role="alert">
                ERROR
            </div>
            `)
            console.log(error);
        });
}

// ==============================UPDATEGETMOVIEDETAIL FUNCTION==================================
async function getMovieDetailUpdate(id) {
    loadingPage();
    const crd_url = `https://api.themoviedb.org/3/movie/${id}/credits?api_key=bd2d4bb5106832ed13fb8fe28dd20430`;
    fetch(crd_url)
        .then((resq) => resq.json())
        .then((json) => {
            let credit = json;
            const detail_url = `https://api.themoviedb.org/3/movie/${id}?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US`;
            fetch(detail_url)
                .then((res) => res.json())
                .then((json) => {
                    $('.row').empty();
                    $('.list-unstyled').empty();
                    let data = json;
                    rev_url = `https://api.themoviedb.org/3/movie/${id}/reviews?api_key=bd2d4bb5106832ed13fb8fe28dd20430&language=en-US&page=1`
                    fetch(rev_url)
                        .then((resp) => resp.json())
                        .then(json => {
                            let reviews = json.results;

                            $('.row').append(`
                    <div class="row no-gutters bg-light position-relative shadow p-1 bg-white rounded">
                    <div class="col-md-6 mb-md-0 p-md-4">
                        <img src="https://image.tmdb.org/t/p/w780/${data.poster_path}" class=" shadow bg-white rounded style="max-width: 29rem;"" alt="${data.title}" height="680px" width="440px">
                    </div>
                    <div class="col-md-6 position-static p-4 pl-md-0">
                        <h1 class=" " role="alert">
                            ${data.title}
                        </h1>
                        <hr/>
                        <p>Genre: ${data.genres[0].name}, ${data.genres[1].name}</p>
                        <p>Overview: ${data.overview}</p>
                        <p>Actors: <a class="text-primary link" style="cursor: pointer;" onclick=getActorDetail(${credit.cast[0].id})>${credit.cast[0].name}</a>, <a class="text-primary link" style="cursor: pointer;"  onclick=getActorDetail(${credit.cast[1].id})>${credit.cast[1].name}</a>, <a class="text-primary link" style="cursor: pointer;"  onclick=getActorDetail(${credit.cast[2].id})>${credit.cast[2].name}</a>, <a class="text-primary link" style="cursor: pointer;"  onclick=getActorDetail(${credit.cast[3].id})>${credit.cast[3].name}</a>...</p>
                        <p>Crews: ${credit.crew[0].name}, ${credit.crew[1].name}, ${credit.crew[2].name}...</p>
                        <p>Original language: ${data.original_language}</p>
                        <p>Spoken languages: ${data.spoken_languages[0].name}</p>
                        <p>Length: ${data.runtime} minutes</p>
                        <p>Released date: ${data.release_date}</p>
                        <p>Production countries: ${data.production_countries[0].name}</p>
                        
                        
                        
                        <a href="index.html" class="btn btn-danger btn-sm p-1 float-right" style="width: 150px;" role="button" aria-pressed="true">Back to home page</a>
                        </div>
                        <p>
                        <button class="btn btn-danger ml-4" type="button" data-toggle="collapse" style="width: 200px;" data-target="#reviewCollapse" aria-expanded="false"   aria-controls="collapseExample">Show/Hide Review</button>
                        </p>
                        <div class="collapse" id="reviewCollapse">
                        <div class="card card-body ml-4 mr-4 mb-4">
                        <p>Authors: ${reviews[0].author}</p>
                        <hr/>
                        <p>${reviews[0].content}</p>
                        </div>
                        </div>
                </div>
            `)

                        })
                })
        })
        .catch(function(error) {
            $('.row').empty();
            $('.list-unstyled').empty();
            $('.list-unstyled').append(`
            <div class="alert alert-danger mx-auto mb-1 p-3" role="alert">
                ERROR
            </div>
            `)
            console.log(error);
        });
}

function loadingPage() {
    $('.row').empty();
    $('.row').append(`
    <div class="d-flex justify-content-center w-100">
    <div class="spinner-grow text-danger" style="width: 4rem; height: 4rem;" role="status">
    <span class="sr-only">Loading...</span>
  </div>
  </div>
      `);
}