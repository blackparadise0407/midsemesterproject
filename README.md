# MidSemesterProject
=================================================================
PHAM DUY DANG KHOA
1712534
=================================================================

Website that includes list of movies and interacts with data.

This is the project completely created by me.

This website can: .display list of popular movies
                  .search movies with given names
                  .search movies with given actors
                  .show the details of a movie
                  .show the details of a actor
                  .show the reviews of movies